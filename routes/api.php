<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* NOTES
 * There is a middleware available to prevent unverified email address from accessing certain routes e.g routes under course prefix
 * The middleware name is 'email.verified'; 
 * Example usage:
 * Route::prefix('course')->namespace('Course')->middleware(['auth:api','email.verified'])->group(function(){
 *
 * });
 * 
 * But for now, we allow users to access all routes under course prefix after successful authentication
 */


Route::prefix('v1')->namespace('Api')->group(function(){
                                                
    // routes for user authentication - login, register, logout, email verify etc 
    Route::prefix('auth')->namespace('Auth')->group(function(){
        Route::post('/login','LoginController@handle');
        Route::post('/register','RegisterController@handle');
        Route::post('/register/seed','RegisterController@seed');
        Route::post('/token/refresh','RefreshTokenController@handle');
        Route::get('/email/verify', 'VerifyEmailController@sendEmailVerfication');
        Route::post('/email/verify', 'VerifyEmailController@handle');
        Route::get('/password/reset', 'ResetPasswordController@sendPasswordResetMail');
        Route::post('/password/reset', 'ResetPasswordController@handle');
        Route::post('/logout', 'LogoutController@handle')->middleware('auth:api');
    });

    // routes for courses - create. list, enroll
    Route::prefix('course')->namespace('Course')->middleware('auth:api')->group(function(){
        Route::post('/', 'CourseController@create');
        Route::post('/enroll', 'CourseController@enroll');
        Route::get('/', 'CourseController@list');
        Route::get('/export', 'CourseController@export');
    });
 
});
