<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>API DOC</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{url('css/app.css')}}" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .page-top{
                padding-top:100px;
            }
        </style>
    </head>
    <body>
        <div class="container page-top">

            <h1>
                Read Me
            </h1>
            
            <br>

            <h2>About</h2>
            <p> This project is a solution to SeamlessHR interview/assessment. First, 3 more self-defined columns were added to the course table, with another migration. 
                Then we have the following endpoints: </p>
            <li>an endpoint for user login and registration - implemented using <code>Passport JWT</code>. Additional endpoints related to authentication were developed as well - endpoints for token refresh, email verification, password update etc were also added.</li>
            <li>an endpoint that calls a course <code>factory</code> to create 50 courses. This operation is <code>queued</code>.</li>
            <li> an endpoint for a user to register in one or more courses.</li>
            <li> an endpoint to see list of all courses, and the date enrolled if the user is registered in that course. </li>
            <li> an endpoint to export all courses with the <code>Maatexcel</code> (any version) package in excel or csv format. </li>

            <br>

            <h2>Requirements</h2>
            <li>Server requirements for <code>Laravel v5.8</code> as found on the official Laravel documentaion website.</li>

            <br>

            <h2>Installation Guide</h2>
            <ul>
                <li>Create a <code>.env</code> file, copy the content in <code>.env.example</code> and paste into the new file </li>
                <li>Fill up the environment variables according to your system configuration. For instance, this project was developed using <code>REDIS</code> as <code>QUEUE CONNECTION</code> and <code>MAILTRAP</code> for <code>MAIL HOST</code> </li>
                <li>Run <code>composer install</code></li>
                <li>Run <code>php artisan key:generate</code></li>
                <li>Run <code>php artisan migrate</code></li>
                <li>Run <code>php artisan passport:install</code>. Copy and keep the keys generated. You will need the password grant client (<code>ID</code> &amp; <code>secret</code>) for login authorization and refresh token</li>
                <li>Confirm that there's a symbolic link of <code>storage/app/public</code> to <code>public/storage</code>, else run<code> php artisan storage:link</code>. This is required to house/keep exported files while a downloadable link is being generated
                <li>Clear cache or run <code>php artisan optimize</code></li>
                <li>Run<code> php artisan serve</code></li>
            </ul>

    
            <br><br>


            <div class="text-center">
                <h2><a href="{{url('/')}}"> Return to Home Page </a></h2>
            </a>

            <br><br>
            
            
        </div>
    </body>
</html>
