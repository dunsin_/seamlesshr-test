<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraColsToCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('title');
            $table->string('requirements')->comment('Requirements or prerequisite for course');
            $table->string('duration')->comment('Estimated timeframe to start and complete course e.g 6 Weeks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('requirements');
            $table->dropColumn('duration');
        });
    }
}