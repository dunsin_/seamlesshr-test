<?php

namespace App\Http\Middleware;

use Closure;
use App\Engine\ApiResources\EmailVerificationStatus;

class EnforceEmailVerification
{

    private $verificationStatus;

    /**
     * Instance of EmailVerificationStatus
     * 
     * 
     */
    public function __construct(EmailVerificationStatus $verificationStatus)
    {
       $this->verificationStatus =  $verificationStatus;
    }

    /**
     * Check whether email is verified
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $this->verificationStatus->handle($request, $next);
    }

}
