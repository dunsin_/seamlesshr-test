<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegistrationRequest extends FormRequest
{
    
    /*
     * Override the "failedValidation" function
     */
    protected function failedValidation(Validator $validator) 
    { 
        throw new HttpResponseException(response()->errorResponse($validator->errors(), 'Request Failed Validation', 422)); 
    }

    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3|max:100',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:35'
        ];
    }

}
