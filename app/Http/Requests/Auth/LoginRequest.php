<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginRequest extends FormRequest
{   

    /*
     * Override the "failedValidation" function
     */
    protected function failedValidation(Validator $validator) 
    { 
        throw new HttpResponseException(response()->errorResponse($validator->errors(), 'Request Failed Validation', 422)); 
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grant_type' => 'required|string|in:password',
            'client_id' => 'required|numeric|min:1',
            'client_secret' => 'required|string',
            'username' => 'required|email',
            'password' => 'required|string',
            'scope' => 'in:*,'
        ];
    }
    
}
