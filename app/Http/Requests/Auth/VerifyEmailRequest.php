<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class VerifyEmailRequest extends FormRequest
{

    /*
     * Override the "failedValidation" function
     */
    protected function failedValidation(Validator $validator) 
    { 
        throw new HttpResponseException(response()->errorResponse($validator->errors(), 'Request Failed Validation', 422)); 
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'token' => 'required|numeric|digits:6'
        ];
    }
}
