<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\IsEmailPresentRequest;
use App\Http\Requests\Auth\VerifyEmailRequest;
use App\Engine\Auth\VerifyEmail;

interface VerifyEmailAddress
{
    public function sendEmailVerfication(IsEmailPresentRequest $isEmailPresentRequest, VerifyEmail $verifyEmail);

    public function handle(VerifyEmailRequest $verifyEmailRequest, VerifyEmail $verifyEmail);
}

class VerifyEmailController extends Controller implements VerifyEmailAddress
{

    /**
     * Send verification email
     * (useful for resending verification token perhaps a user fails to receive initially sent email)
     *
     * @return [json] 
     */
    public function sendEmailVerfication(IsEmailPresentRequest $isEmailPresentRequest, VerifyEmail $verifyEmail)
    {
        return $verifyEmail->sendEmailVerfication($isEmailPresentRequest);
    }


    /**
     * Confirm email verification  
     *
     * @return [json] 
     */
    public function handle(VerifyEmailRequest $verifyEmailRequest, VerifyEmail $verifyEmail)
    {
        return $verifyEmail->handle($verifyEmailRequest);
    }

}


