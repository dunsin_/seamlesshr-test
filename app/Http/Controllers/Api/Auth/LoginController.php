<?php

namespace App\Http\Controllers\Api\Auth;

use Psr\Http\Message\ServerRequestInterface;
use App\Http\Requests\Auth\LoginRequest;
use App\Engine\Auth\Login;

interface LoginUser
{
    public function handle(ServerRequestInterface $request, LoginRequest $loginRequest, Login $login);
}

class LoginController implements LoginUser
{   
    
    /** 
     * Login user (issue access token)
     * 
     * @return [json] 
     */
    public function handle(ServerRequestInterface $request, LoginRequest $loginRequest, Login $login)
    {
        return  $login->handle($request);
    }

}
