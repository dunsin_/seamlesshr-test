<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Engine\Auth\Logout;

interface LogoutUser
{
    public function handle(Logout $logout);
}

class LogoutController extends Controller implements LogoutUser
{

    /**
     * Logout user
     *
     * @return [json] 
     */
    public function handle(Logout $logout)
    {
        return $logout->handle();
    }

}
