<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\IsEmailPresentRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Engine\Auth\ResetPassword;

interface PasswordResetter
{
    public function sendPasswordResetMail(IsEmailPresentRequest $isEmailPresentRequest, ResetPassword $resetPassword);

    public function handle(ResetPasswordRequest $resetPasswordRequest, ResetPassword $resetPassword);
}

class ResetPasswordController extends Controller implements PasswordResetter
{

    /**
     * Send password reset email 
     *
     * @return [json] 
     */
    public function sendPasswordResetMail(IsEmailPresentRequest $isEmailPresentRequest, ResetPassword $resetPassword)
    {
        return $resetPassword->sendPasswordResetMail($isEmailPresentRequest);
    }

    /**
     * Reset user's password 
     *
     * @return [json] 
     */
    public function handle(ResetPasswordRequest $resetPasswordRequest, ResetPassword $resetPassword)
    {
        return $resetPassword->handle($resetPasswordRequest);
    }

}
