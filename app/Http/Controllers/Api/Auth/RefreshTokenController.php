<?php

namespace App\Http\Controllers\Api\Auth;

use Psr\Http\Message\ServerRequestInterface;
use App\Http\Requests\Auth\RefreshTokenRequest;
use App\Engine\Auth\TokenRefresh;

interface RefreshToken
{
    public function handle(ServerRequestInterface $request, RefreshTokenRequest $refreshTokenRequest, TokenRefresh $tokenRefresh);
}

class RefreshTokenController implements RefreshToken
{

    /**
     * Refresh user's token
     *
     * @return [json] 
     */
    public function handle(ServerRequestInterface $request, RefreshTokenRequest $refreshTokenRequest, TokenRefresh $tokenRefresh)
    {
        return $tokenRefresh->handle($request);
    }

}
