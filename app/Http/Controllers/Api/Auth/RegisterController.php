<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Engine\Auth\Register;

interface RegisterUser
{
    public function handle(RegistrationRequest $registrationRequest, Register $register);
}

class RegisterController extends Controller implements RegisterUser
{

    /**
     * Register user 
     * (create new user and send email verification token)
     *
     * @return [json] 
     */
    public function handle(RegistrationRequest $registrationRequest, Register $register)
    {
        return $register->handle($registrationRequest);
    }

    /**
     * Register user using seeding
     * (create new user and auto verify email)
     *
     * @return [json] 
     */
    public function seed(Register $register)
    {
        return $register->seedRegister();
    }

}
