<?php

namespace App\Http\Controllers\Api\Course;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\CreateCourses;
use App\Http\Requests\Course\EnrollRequest;
use App\Engine\Course\EnrollCourses;
use App\Engine\Course\ListCourses;
use App\Engine\Course\ExportCourses;

class CourseController extends Controller
{
    
    public function create()
    {
        $handle = CreateCourses::dispatch();  // queue that creates 50 courses using a course factory
        return 
        ( 
            $handle? 
            response()->successResponse(null, 'The process of creating courses has been initiated', 200) : 
            response()->errorResponse(null, 'Attempt to initiate course creation failed', 400)
        );
    }

    public function enroll(EnrollRequest $enrollRequest, EnrollCourses $enrollCourses)
    {
        return $enrollCourses->handle($enrollRequest);
    }

    public function list(ListCourses $listCourses)
    {
        return $listCourses->handle();
    }

    public function export(ExportCourses $exportCourses)
    {
        return $exportCourses->handle();
    }

}
