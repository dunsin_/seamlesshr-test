<?php

namespace App\Engine\Auth;

use App\Notifications\ApiEmailVerification;
use Carbon\Carbon;

use Log;
use Exception;

use App\User;

class VerifyEmail
{

    /**
     * Email verification token
     * (useful for resending verification token when a user fails to receive initially sent email)
     *
     * @return [json] 
     */
    public function sendEmailVerfication($request)
    {
        try 
        {
            $token = mt_rand(100000, 999999); //generate a 6 digit token
            $updateToken = User::where('email',$request->email)->update(['remember_token'=>$token, 'updated_at'=>now()]);
            if($updateToken)
            {
                $user = User::where('email',$request->email)->select('id','email','email_verified_at','remember_token')->first();
                
                if(isset($user->email_verified_at))
                return response()->errorResponse(null, 'This user has been verified already. No further action is required', 428);
                
                $user->notify(new ApiEmailVerification($user));

                return 
                ( 
                    $user? 
                    response()->successResponse(['email'=>$user->email], 'Verification token has been emailed', 200) : 
                    response()->errorResponse(null, 'Attempt to email verification token failed', 400)
                );
            }
        
            return response()->errorResponse(['email'=>$request->email], 'Failed attempt to generate verification token for this email', 400);
            
        }
        catch (Exception $e) 
        {
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }

    }

    
    /**
     * Confirm/Verify user 
     * (update email_verified_at for user) 
     *
     * @return [json] 
     */
    public function handle($request)
    {
        try 
        {
            $email = $request->email;
            $token = $request->token;

            $user = User::select('remember_token','email_verified_at','updated_at')->where('email',$email)->first();

            if(empty($user))
            return response()->errorResponse(null, "We couldn't identify the supplied email address", 400);
            
            if(isset($user->email_verified_at))
            return response()->errorResponse(null, 'This user has been verified already. No further action is required', 428);

            if($token != $user->remember_token)
            return response()->errorResponse(null, 'This token is invalid', 400);
            
            $tokenCreatedAt = Carbon::parse($user->updated_at);
            $now = now();
            $timeDiff = $tokenCreatedAt->diffInMinutes($now);
            
            if($timeDiff <= 15)
            {   
                $verified = User::where('email',$email)->update(['remember_token'=>null, 'email_verified_at'=>now(), 'updated_at'=>now()]);
                return (
                    ($verified)?
                    response()->successResponse(null, 'User verification successful', 200) :     
                    response()->errorResponse(null, 'Failed to verify user', 400)      
                );
                
            }
            
            return response()->errorResponse(['error'=>'Token has elapsed the fifteen minutes validity period. Kindly request for a new token'], 'Token is no longer valid', 400);
        } 
        catch (Exception $e) 
        {
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
    }

}
