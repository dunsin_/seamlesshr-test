<?php

namespace App\Engine\Auth;

use App\Notifications\ApiEmailVerification;

use Log;
use Exception;

use App\User;

class Register 
{

    /**
     * Register user (create new user and send email verification link)
     *
     * @return [json] 
     */
    public function handle($request)
    {
        try 
        {
            $input = $request->all(); 
            $input['password'] = bcrypt($input['password']); 
            $input['remember_token'] = mt_rand(100000, 999999); // generate a 6 digit token
            
            $user = User::create($input); 
            $user->notify(new ApiEmailVerification($user)); // uses queue

            return 
            ( 
                $user? 
                response()->successResponse(null, 'User successfully created and verification token emailed!', 201) : 
                response()->errorResponse(null, 'Attempt to create user failed', 400)
            );
            
        }
        catch (Exception $e) 
        {
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
        
    }

    /**
     * Register user using Userfactory
     *
     * @return [json] 
     */
    public function seedRegister()
    {
        try 
        {
            $user = factory(User::class)->create();
            
            return 
            ( 
                $user? 
                response()->successResponse(['email'=>$user->email, 'password'=>'123456'], 'User successfully created!', 201) : 
                response()->errorResponse(null, 'Attempt to create user failed', 400)
            );
            
        }
        catch (Exception $e) 
        {
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
    }

}
