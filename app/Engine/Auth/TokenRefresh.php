<?php

namespace App\Engine\Auth;

use Laravel\Passport\Http\Controllers\AccessTokenController as ATC;

use Log;
use Exception;

use App\User;

class TokenRefresh extends ATC
{   
    
    /** 
     * Refresh user token (issues a new  access token)
     * 
     * @return [json] 
     */
    public function handle($request)
    {
        try
        {
            //generate token
            $tokenResponse = parent::issueToken($request);

            //convert response to json string
            $content = $tokenResponse->getContent();

            //convert json to array
            $data = json_decode($content, true);

            //check if an error occured
            if(isset($data['error']))
            {
                if(isset($data['message']))
                {
                    $customErrorMsg = ['invalid_client'=>'Unknown client', 'invalid_request'=> 'Invalid Token. Token has been revoked'];
                    $message = $customErrorMsg[ strtolower($data['error']) ];
                    $errorData = [ $data['error']=>$data['message'] ];
                    return response()->errorResponse($errorData, $message, 401);
                }
                return response()->errorResponse(null, $data['error'], 401);
            }

            //create a collection
            $response = collect([]);
            
            //params to be send out in the response body
            $response->put('access_token', $data['access_token']);
            $response->put('token_type', $data['token_type']);
            $response->put('expires_in', $data['expires_in']);
            $response->put('refresh_token', $data['refresh_token']);

            return response()->successResponse($response, 'Token refreshed successfully', 200);
        }
        catch (Exception $e) 
        { 
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
    }

}
