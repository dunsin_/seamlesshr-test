<?php

namespace App\Engine\Auth;

use Illuminate\Support\Facades\Hash;
use App\Notifications\ApiResetPassword;
use Carbon\Carbon; 

use Log;
use Exception;

use App\User;

class ResetPassword
{
    
    /**
     * Send password reset email
     *
     * @return [json] 
     */
    public function sendPasswordResetMail($request)
    {
        try 
        {
            $token = mt_rand(100000, 999999); //generate a 6 digit token
            $updateToken = User::where('email',$request->email)->update(['remember_token'=>$token, 'updated_at'=>now()]);
            if($updateToken)
            {
                $user = User::where('email',$request->email)->select('id','email','remember_token')->first();
                $user->notify(new ApiResetPassword($user));

                return 
                ( 
                    $user? 
                    response()->successResponse(['email'=>$user->email], 'Password reset token has been emailed', 200) : 
                    response()->errorResponse(null, 'Attempt to email password reset token failed', 400)
                );
            }
        
        return response()->errorResponse(['email'=>$request->email], 'Failed attempt to generate password reset token for this email', 400);
            
        }
        catch (Exception $e) 
        {
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }

    }

    
    /**
     * Reset password for user 
     *
     * @return [json] 
     */
    public function handle($request)
    {
        try 
        {
            $email = $request->email;
            $token = $request->token;

            $user = User::select('remember_token','updated_at')->where('email',$email)->first();

            if(empty($user))
            return response()->errorResponse(null, "We couldn't identify the supplied email address", 400);

            if($token != $user->remember_token)
            return response()->errorResponse(null, 'This token is invalid', 400);
            
            $tokenCreatedAt = Carbon::parse($user->updated_at);
            $now = now();
            $timeDiff = $tokenCreatedAt->diffInMinutes($now);
            
            if($timeDiff <= 15)
            {   
                $password = Hash::make($request->password);
                $passwordUpdated = User::where('email',$email)->update(['remember_token'=>null, 'password'=>$password, 'updated_at'=>now()]);
                return 
                (
                    $passwordUpdated?
                    response()->successResponse(null, 'Password reset was successful', 200) :     
                    response()->errorResponse(null, 'Attemp to reset password failed', 400)      
                );
                
            }
            
            return response()->errorResponse(['error'=>'Token has elapsed the fifteen minutes validity period. Kindly request for a new token'], 'Token is no longer valid', 400);
        } 
        catch (Exception $e) 
        {
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
    }

}
