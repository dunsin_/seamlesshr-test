<?php

namespace App\Engine\Auth;

use Laravel\Passport\Http\Controllers\AccessTokenController as ATC;

use Log;
use Exception;

class Login extends ATC 
{   
    
    /** 
     * Login user (issue access token)
     * 
     * @return [json] 
     */
    public function handle($request)
    {
        try
        {
            //generate token, convert to json string and then to array
            $tokenResponse = parent::issueToken($request);
            $content = $tokenResponse->getContent();
            $data = json_decode($content, true);

            //check if an error occured
            if(isset($data['error']))
            {
                if(isset($data['message']))
                {
                    $customErrorMsg = ['invalid_client'=>'Unknown client', 'invalid_credentials'=> 'Incorrect email or password'];
                    $message = $customErrorMsg[ strtolower($data['error']) ];
                    $errorData = [ $data['error']=>$data['message'] ];
                    return response()->errorResponse($errorData, $message, 401);
                }
                return response()->errorResponse(null, $data['error'], 401);
            }

            //create a collection
            $response = collect([]);
            
            //params to be send out in the response body
            $response->put('access_token', $data['access_token']);
            $response->put('token_type', $data['token_type']);
            $response->put('expires_in', $data['expires_in']);
            $response->put('refresh_token', $data['refresh_token']);
            
            return response()->successResponse($response, 'Login successful', 200);
        }
        catch (Exception $e) 
        { 
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
    }

}
