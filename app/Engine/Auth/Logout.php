<?php

namespace App\Engine\Auth;

use Log;
use Exception;

class Logout
{
    
    /**
     * Logout user (revoke the token)
     *
     * @return [json] 
     */
    public function handle()
    {
        try
        {
            return 
            (
                request()->user()->token()->revoke()?
                response()->successResponse(null, 'Successfully logged out', 200):
                response()->errorResponse(null, 'Attempt to logout user failed', 400)
            );
        }
        catch (Exception $e) 
        { 
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
    }
  
}
