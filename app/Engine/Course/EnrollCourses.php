<?php

namespace App\Engine\Course;

use Log;
use Exception;

use App\Enrollment;

class EnrollCourses
{
    
    /**
     * Enroll courses
     *
     * @return [json] 
     */
    public function handle($request)
    {
        try
        {
            $enrollIn = $request->enroll_in;
            $userId = request()->user()->id;

            $dataToInsert = [];
            $i = 0;
            foreach($enrollIn as $courseId)
            {
                $dataToInsert[$i] = ['user_id'=>$userId, 'course_id'=>$courseId, 'created_at'=>now()];
                $i++;
            }

            $save = Enrollment::insert($dataToInsert);
            return 
            (
                $save?
                response()->successResponse(null, "Course registration was successful", 200):
                response()->errorResponse(null, 'Attempt to register course failed', 400)
            );
        }
        catch (Exception $e) 
        { 
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
    }
  
}
