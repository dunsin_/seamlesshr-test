<?php

namespace App\Engine\Course;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Facades\Excel;

use App\Course;

class ExportCourses implements FromCollection
{

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Course::all();
    }

    public function handle() 
    {
        $format = strtolower(filter_var(request('format'), FILTER_SANITIZE_STRING) ?? 'csv'); // sanitize string coming from GET request and convert to lower case
        if($format != 'csv' AND $format != 'xlsx')
        return response()->errorResponse(null, 'Invalid file format supplied. Supported formats are xlsx or csv', 400);

        $filename = time().".".$format;
        $storeFile = Excel::store(new ExportCourses, $filename,'public');
        $downloadLink = url('storage/'.$filename);

        return 
        (
            $storeFile?
            response()->successResponse(['download_link'=>$downloadLink], "Courses exported successfully. The exported file is available for download at ".$downloadLink, 200):
            response()->errorResponse(null, 'Attempt to download courses failed', 400)
        );
    }

}
