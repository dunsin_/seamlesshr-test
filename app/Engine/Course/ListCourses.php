<?php

namespace App\Engine\Course;

use Log;
use Exception;

use App\Course;

class ListCourses
{
    
    /**
     * List Courses
     * (plus display date enrolled for if user is registered in that course)
     *
     * @return [json] 
     */
    public function handle()
    {
        try
        {
            $userId = request()->user()->id;
            $columns = ['courses.id', 'courses.title', 'courses.requirements', 'courses.duration', 'courses.text',  'course_enrollments.user_id', 'course_enrollments.created_at'];
            $courses = Course::select($columns)->leftJoin('course_enrollments','courses.id','=','course_enrollments.course_id')->get();
            
            foreach($courses as $course)
            {
                if($userId==$course->user_id) // user has enrolled in this course
                {
                    $course->has_enrolled = true;
                    $course->date_enrolled = $course->created_at;
                }
                else
                {
                    $course->has_enrolled = false;
                    $course->date_enrolled = null;
                } 
                unset($course->user_id);
                unset($course->created_at);
            }

            return 
            (
                $courses?
                response()->successResponse($courses, "Courses retrieved successfully", 200):
                response()->errorResponse(null, 'Attempt to retrieve courses failed', 400)
            );
        }
        catch (Exception $e) 
        { 
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
    }
  
}
