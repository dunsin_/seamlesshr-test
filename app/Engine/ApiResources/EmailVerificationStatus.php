<?php

namespace App\Engine\ApiResources;

use Illuminate\Http\Request;

use Log;
use Exception;

class EmailVerificationStatus
{
    
    /** 
     * Check email verification status
     * 
     * @return [json] 
     */
    public function handle($request, $next)
    {
        try
        {
            return 
            isset(request()->user()->email_verified_at)?
            $next($request) :
            response()->errorResponse(null, 'To proceed with this request, verify your email address', 400);
        }
        catch(Exception $e)
        {
            Log::error("Exception ".$e);
            return response()->errorResponse(null, 'Internal server error', 500);
        }
        
    }
        
}
