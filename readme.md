# Read Me

## About SeamlessHR-test

This project is a solution to SeamlessHR interview/assessment. First, 3 more self-defined columns were added to the course table, with another migration. Then we have the following endpoints: 

- an endpoint for user login and registration - implemented using `Passport JWT`. Additional endpoints related to authentication were developed as well - endpoints for token refresh, email verification, password update etc were also added.
- an endpoint that calls a course `factory` to create 50 courses. This operation is `queued`.
- an endpoint for a user to register in one or more courses.
- an endpoint to see list of all courses, and the date enrolled if the user is registered in that course. 
- an endpoint to export all courses with the `Maatexcel` (any version) package in excel or csv format. 

## Requirements
- Server requirements for `Laravel v5.8` as found on the official Laravel documentaion website.

## Installation Guide

- Create a `.env` file, copy the content in `.env.example` and paste into the new file 
- Fill up the environment variables according to your system configuration. For instance, this project was developed using `REDIS` as `QUEUE CONNECTION` and `MAILTRAP` for `MAIL HOST` 
- Run `composer install`
- Run `php artisan key:generate`
- Run `php artisan migrate`
- Run `php artisan passport:install`. Copy and keep the keys generated. You will need the password grant client (`ID` & `secret`) for login authorization and refresh token. Note: If you are deploying Passport to your production servers for the first time, you likely need to run `php artisan passport:keys`
- Confirm that there's a symbolic link of `storage/app/public` to `public/storage`, else run` php artisan storage:link`. This is required to house/keep exported files while a downloadable link is being generated
- Clear cache or run `php artisan optimize`
- Run` php artisan serve`

## Documentation & Usage
API Documentaion link: [documenter.getpostman.com/view/8944386/SzRxVpvA](https://documenter.getpostman.com/view/8944386/SzRxVpvA)

## Developer: Dunsin Olubobokun
Website: [dunsinolubobokun.com](http://dunsinolubobokun.com/)
